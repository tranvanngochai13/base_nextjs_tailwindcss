import "../styles/globals.css";
import { Provider } from "react-redux";
import { store } from "../store";
import { MainLayout } from "../layout";

function MyApp(props) {
  const { Component, pageProps } = props;
  const Layout = Component.Layout ?? MainLayout;

  return (
    <Provider store={store}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  );
}

export default MyApp;
